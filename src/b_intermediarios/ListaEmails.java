package b_intermediarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Crie um programa que lê os clientes presentes no arquivo contas.csv e filtre
 * aqueles que possuem o saldo superior à 7000. Desses clientes, criar outro
 * arquivo de texto que possua todos os clientes no seguinte formato:
 * 
 * João da Silva<joaosilva@teste.com>, Maria da Penha<maria@teste.com>
 * 
 */
public class ListaEmails {
	public static void main(String[] args) {
		Path path = Paths.get("/home/mastertech/Documentos/contas.csv");
		Path pathFormat = Paths.get("/home/mastertech/Documentos/contasPremium.csv");
		StringBuilder emails = new StringBuilder();
		
		try {
			List<String> lista = Files.readAllLines(path);
			lista.remove(0);
//			for(int i=0; i < lista.size(); i++) {
//				System.out.println(lista.get(i));
//			}
			
			for(String linha: lista) {
				String [] partes = linha.split(",");
				
				int saldo = Integer.parseInt(partes[4]);
				
				if(saldo > 7000) {
					String email = String.format("%s %s<%s>, ", partes[1], partes[2], partes[3]);
					emails.append(email);
				}
			}
			System.out.println(emails);
			byte [] listaBytes = emails.toString().getBytes();
			Files.write(pathFormat, listaBytes);
			
//			lista.forEach(linha -> System.out.println(linha));
			
		}catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}

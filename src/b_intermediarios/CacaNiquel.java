package b_intermediarios;

import java.util.Random;

/**
 * Crie um programa que simula o funcionamento de um caça níquel, da seguinte forma:
 * 
 * 1- Sorteie 3x um valor dentre as strings do vetor de valores
 * 2- Compare os valores sorteados e determine os pontos obtidos usando o
 * seguinte critério:
 * 
 * Caso hajam três valores diferentes: 0 pontos
 * Caso hajam dois valores iguais: 100 pontos
 * Caso hajam três valores iguais: 1000 pontos
 * Caso hajam três valores "7": 5000 pontos
 * 
 */
public class CacaNiquel {
    public static void main(String args[]) {
        String[] valores = {"abacaxi", "framboesa", "banana", "pera", "7"};
        
        Random random = new Random();
        int sorteio;
        
        String [] sort = new String[3];
        
        for(int i=0; i < sort.length; i++) {
            sorteio = random.nextInt(5);
            sort[i] = valores[sorteio];
            System.out.println(sort[i]);
        }
        
        
        if((sort[0] != sort[1]) && (sort[1] != sort[2]) && (sort[2] != sort[0]) ) {
            System.out.println("Voce fez 0 pontos!");
        }else if ((sort[0].equals(sort[1])) && (sort[1].equals(sort[2]))) {
            System.out.println("Voce fez 1000 pontos!");
        }else if ((sort[0].equals(sort[1])) || (sort[1].equals(sort[2])) || (sort[2].equals(sort[0]))) {
            System.out.println("Voce fez 100 pontos!");
        }else if ((sort[0].equals("7")) && (sort[1].equals("7")) && (sort[2].equals("7"))) {
            System.out.println("Voce fez 5000 pontos!");
        }
        
    }
}
 



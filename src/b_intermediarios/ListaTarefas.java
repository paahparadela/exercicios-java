package b_intermediarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Crie um programa que recebe um todo list e a escreve em um arquivo txt.
 * 
 * A cada iteração, o sistema deve pedir uma  única tarefa e exibir a possibilidade
 * de encerrar o programa.
 * 
 * Quando o usuário encerrar o programa, deve-se gerar o arquivo txt com as tarefas
 * que foram inseridas.
 *
 */
public class ListaTarefas {
	public static void main(String[] args) {
		Path path = Paths.get("/home/mastertech/Documentos/tarefas.txt");
		
		Scanner ler = new Scanner(System.in);
		String menu = "";
		StringBuilder lista = new StringBuilder();
		
		while (!menu.equals("0")) {
			System.out.println("Digite uma tarefa ou 0 para sair: ");
			menu = ler.nextLine();
			
			if(!menu.equals("0")) {
				lista.append(menu + "\n");
			}
		}
		
		try {
			byte [] listaBytes = lista.toString().getBytes();
			Files.write(path, listaBytes);
			System.out.println("Lista de tarefas se encontra em: "+path);
		}catch(IOException e) {
			e.printStackTrace();
		}
		
	}
}

package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe um número do usuário
 * e imprime a fatorial desse mesmo número.
 * 
 */
public class Fatorial {
	public static void main(String args[]) {
		Scanner ler = new Scanner(System.in);
		System.out.print("Digite um numero: ");
		int num = ler.nextInt();
		System.out.println();
		int cont = num;
		int fat = num;
		
		for(int i = 1; i < num; i++) {
			fat = fat * --cont;
		}
		
		System.out.print("Fatorial de " + num + " eh " + fat);
		
		ler.close();
	}
}

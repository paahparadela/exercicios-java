package a_basicos;

/**
 * Crie um programa que imprima todas as cartas do baralho.
 * 
 * Exemplo: 
 * 
 * Ás de Ouros
 * Ás de Espadas
 * Ás de Copas
 * Ás de Paus
 * Dois de Ouros
 * ...
 * 
 */
public class Baralho {
	public static void main (String[] args) {
		String [] valores = {"As", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Valete", "Dama", "Rei"};
		String [] naipes = {"Paus", "Ouros", "Copas", "Espadas"};
		
		for(String valor: valores) {
			for(String naipe: naipes) {
				System.out.println(valor + " de " + naipe);
			}
		}
		
	}

}
package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe uma palavra do terminal e determina
 * se ela é um palíndromo.
 * 
 * Exs: 
 * 
 * input: ovo
 * output: A palavra ovo é um palíndromo
 * 
 * input: jose
 * output: A palavra jose não é um palíndromo 
 */
public class Palindromo {
	public static void main(String args[]) {
		
		Scanner ler = new Scanner(System.in);
		
		System.out.println("Digite uma palavra");
		
		String palavraUser = ler.nextLine();
		
		String palavraResul = "";
		
		String [] palindromo = new String [palavraUser.length()];
		
		for(int i=0; i < palindromo.length; i++) {
			palindromo[i] = palavraUser.substring(i, i+1);
		}
		
		int cont = palindromo.length;
		
		for(int k=0; k < palindromo.length; k++) {
			palavraResul = palavraResul + palindromo[--cont];
		}
		
		if(palavraUser.equals(palavraResul)) {
			System.out.println("Eh palindromo");
		}else {
			System.out.println("Nao eh palindromo");
		}
		
	}
}

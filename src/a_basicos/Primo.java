package a_basicos;

public class Primo {
	/**
	 * Crie um programa que imprima todos os números
	 * primos de 1 à 100
	 */
	public static void main(String[] args) {
		int cont=0;
		System.out.println("Numeros primos de 1 a 100");
		for (int i=2; i <= 100; i++) {
			for (int k=1; k<=10; k++) {
				if(i%k==0) {
					cont++;
				}
			}
			if(cont<=2) {
				System.out.println(i);
			}
			cont = 0;
		}
		
	}
}
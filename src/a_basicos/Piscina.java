package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe as três dimensões de uma piscina
 * (largura, comprimento, profundidade) e retorne a sua capacidade
 * em litros.
 */
public class Piscina {
	public static void main(String[] args) {
		Scanner ler = new Scanner(System.in);
		System.out.println("Digite a largura da piscina: ");
		int largura = ler.nextInt();
		System.out.println("Digite o comprimento da piscina: ");
		int comprimento = ler.nextInt();
		System.out.println("Digite a profundidade da piscina: ");
		int profundidade = ler.nextInt();
		
		int volume = largura * comprimento * profundidade;
		
		volume = volume *1000;
		
		System.out.println("O volume da piscina eh " + volume + " litros");
		
		ler.close();
		
	}
}

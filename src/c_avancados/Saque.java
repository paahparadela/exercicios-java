package c_avancados;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/**
 * Crie um programa que faça um saque no arquivo contas.csv. O programa deve
 * receber um id e o valor do saque. Ele deve informar caso o id não seja
 * encontrado ou caso o valor não esteja disponível.
 * 
 * Após o saque, o arquivo deve ser atualizado.
 * 
 */
public class Saque {
	public static void Menu() {
		System.out.println("************ Menu ************");
		System.out.println("1. Sacar");
		System.out.println("0. Sair");
	}

	public static void main(String[] args) throws IOException {
		String menu = "";
		Scanner ler = new Scanner(System.in);
		Path path = Paths.get("/home/mastertech/Documentos/contas_cp.csv");
		int saque, saldo, novoSaldo;
		String conta;

		while (!menu.equals("0")) {
			Menu();
			menu = ler.nextLine();
			if (!menu.equals("0")) {
				if (menu.equals("1")) {
					System.out.print("Digite o número da sua conta: ");
					conta = ler.nextLine();
					System.out.println();
				

					List<String> lista = Files.readAllLines(path);
					//lista.remove(0);
					
					for (int i=1; i<lista.size(); i++) {
						
						String[] partes = lista.get(i).split(",");

						if (conta.equals(partes[0])) {
							System.out.println("BEM VINDO SR(A) " + partes[1]);
							System.out.print("Digite o valor do saque: ");
							menu = ler.nextLine();
							System.out.println();
							saque = Integer.parseInt(menu);
							saldo = Integer.parseInt(partes[4]);
							
							if (saque > saldo) {
								System.out.println("O valor do saque é maior que o saldo disponível na conta-corrente");
								//return;
							} else {
								novoSaldo = saldo - saque;
								String alteracao = String.format("%s,%s,%s,%s,%s", partes[0], partes[1], partes[2],
										partes[3], novoSaldo);
								lista.set(i, alteracao);
								System.out.println("Valor sacado: " + saque);
								System.out.println("Saque efetuado com sucesso!");
							}
							
						}
					}
					
					String inclusao = "";
					for(String linha: lista) {
						inclusao += linha + "\n";
					}
					Files.write(path, inclusao.getBytes());
				}
			}
		}
		System.out.println("Fim do Programa");

	}
}
